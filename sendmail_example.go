package sendmail

import (
	"log"
	"os"
	"testing"
)

func ExampleNewEmail(t *testing.T) {
	err := NewEmail(RequestResetPassword).
		To(&Address{Email: "recipient@domain.com", Name: "Recipient Name"}).
		Params(&Params{
			"name":      "Recipient Name",
			"link":      "http://localhost:8090/link?token=token",
			"staffName": "Acarya",
			"staffMail": "acarya@skillogs.com",
			"staffUrl":  "http://app.skillogs.com",
		}).Send(os.Getenv("AMQP_URI"), "skillogs.dev.emails.e")
	if err != nil {
		log.Fatal(err)
	} else {
		log.Println("Mail sent")
	}
}
